| Title                                                                     | Author          | Type       | Estimated time | Software |
|---------------------------------------------------------------------------+-----------------+------------+----------------+----------|
| [Making a Map](https://www.qgistutorials.com/en/docs/3/making_a_map.html) | Ujaval Gandhi   | Tutorial   | 30 minutes     | QGIS     |
| [Critical assessment of maps](https://libregeo.com)                       | Matthew Haffner | Activity   | 15 minutes     | NA       |
| [Analyze two web maps](https://www.e-education.psu.edu/geog585/node/687)  | Sterling Quinn  | Assignment | 25 minutes     | NA       |
