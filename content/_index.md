---
date: "2016-05-05T21:48:51-07:00"
title: Libre Geo
---

# Libre Geo

This website contains resources for the Libre Geo project, a collection of
geographic information science and technology (GIS&T) course materials. Libre
Geo's focus is on in-class activities, out-of-class exercises, and interactive
classroom instruction.

### Project goals and non-goals

Libre Geo is not meant to be a "drag-and-drop" textbook for a single GIS&T
course. Rather, it is meant to be a comprehensive repository of many different
teaching resources. The best approach is determine which concepts you would like
to teach and extract select resources geared to those concepts. Libre Geo takes
a "building block" approach, partly inspired by the (completely unrelated)
status bars of tiling window managers like `libqtile.bar` and `i3-blocks`: You
choose which elements work for you, modify them to your liking, and quickly
incorporate them into your teaching.

### Motivation

The Libre Geo project is motivated by GIS educators who:

- Want a more collaborative teaching experience with others instructing on
  similar content
- Have experienced the difficulty of creating numerous high-quality teaching
  exercises for multiple geospatial courses
- Have become disillusioned with _physical_ textbooks geared toward rapidly
  changing software (GIS)
- Are dissatisfied with the educational resources provided by other GIS
  textbooks
- Want to create and use "libre" (i.e. free as in free speech) and open access
  (i.e. free as in free beer) course materials for students and other educators

### Organization

The project is organized as follows: topics are listed on the left-hand
navigation pane. Topics contain subtopics, and within those are various types of
resources, called "modules": (a) short activities, (b) tutorials, \(c) and
in-depth exercises.

## How to use Libre Geo

Instructors can use these resources in several different ways. These are
organized in descending order from most basic (and simplest to implement) to
most fully featured (though more complex). 

- **Method A**: Informally graze this website and incorporate the ideas into your
coursework as desired. This approach is self-explanatory.

- **Method B**: Download a `.zip` of the `GitLab` repository, copy various `.Rmd` files from the
[GitLab repository](https://gitlab.com/mhaffner/libre-geo), and work them into
your own course materials.

- **Method C**: Fork the GitLab repository
`https://gitlab.com/mhaffner/libre-geo`, make changes and modifications as
desired, and deploy your version of the repository using GitLab pages. Ideally,
we'd like to have your changes and contributions back! Currently, the collection
of resources in Libre Geo is being deployed using this method.

## Contributing

As a collaborative project, contributions are highly encouraged but certainly
not obligatory. Procedures on contributing a module (or simply improving the
website) can be found [here](https://gitlab.com/mhaffner/libre-geo).
